import React, {useState} from 'react';
import './App.css';
import Modal from "./components/UI/Modal/Modal";

const App = () => {

  const [purchasing, setPurchasing] = useState(false);

  const purchaseHandler = () => {
    setPurchasing(true);
  };

  const purchaseCancelHandler = () => {
    setPurchasing(false);
  };

  return (
      <div className="App">
        <Modal
            show={purchasing}
            canceled={purchaseCancelHandler}
            title="Some Title"
        >
          <p>Some text</p>
        </Modal>

        <button className="show-modal-btn" onClick={purchaseHandler}>Show Modal</button>
      </div>
  );
}

export default App;
