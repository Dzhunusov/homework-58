import React from 'react';
import './Modal.css';
import Backdrop from "../Backrdop/Backdrop";

const Modal = props => (
    <>
      <Backdrop show={props.show} clicked={props.closed}/>
      <div className="Modal"
           style={{
             transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
             opacity: props.show ? '1' : '0'
           }}
      >
        <h3 className="modal-title">{props.title}</h3>
        {props.children}
        <button className="cancel-btn"  onClick={props.canceled}>X</button>
      </div>
    </>
);

export default Modal;